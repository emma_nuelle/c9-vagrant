#!/bin/bash

# ------------------------------------------------------------------------------
# Install base
apt-get update -qy
apt-get install -qy --force-yes git vim build-essential g++ curl libssl-dev apache2-utils software-properties-common unzip wget sudo tmux hostname supervisor

# ------------------------------------------------------------------------------
# Install extra python packages
add-apt-repository -y ppa:fkrull/deadsnakes
apt-get update -qy
apt-get install -qy --force-yes python2.7-dev python3.5 python3.5-dev python-pip python-virtualenv postgresql-server-dev-9.3 libjpeg-dev libjpeg-turbo8-dev libjpeg8-dev libxml2-dev libxslt1-dev

# ------------------------------------------------------------------------------
# Install Node.js
curl -sL https://deb.nodesource.com/setup_5.x | bash -
apt-get install -qy --force-yes nodejs


# Add bower and ember-cli
npm install -g bower ember-cli

# Add cookiecutter
pip install cookiecutter

# ------------------------------------------------------------------------------
# Update .bashrc for vagrant + access rights
sed -i 's/#force_color_prompt=yes/force_color_prompt=yes/g' /home/vagrant/.bashrc
echo "vagrant ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
if [ -d /c9sdk ]; then
  rm -rf /c9sdk
fi
git clone https://github.com/c9/core.git /c9sdk
cd /c9sdk
chown -R vagrant /c9sdk
su -c "scripts/install-sdk.sh" vagrant

# ------------------------------------------------------------------------------
# Clean up APT when done.
apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*


