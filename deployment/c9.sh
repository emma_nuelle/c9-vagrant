#!/bin/bash
# ------------------------------------------------------------------------------
# Downloading Cloud9

cd /c9sdk
sed -i 's/https/http/g' /c9sdk/plugins/c9.ide.preview/preview.js
sed -i 's/standalone: true/appHostname: "localhost:4200", standalone:true/g' /c9sdk/settings/standalone.js
cd ~
curl -L https://raw.githubusercontent.com/c9/install/master/install.sh | bash
