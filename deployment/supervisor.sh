#!/bin/bash
cp /vagrant/scripts/c9.sh /c9sdk/
chown vagrant /c9sdk/c9.sh
cp /vagrant/conf/c9.conf /etc/supervisor/conf.d
supervisorctl update
