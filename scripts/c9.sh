#!/bin/bash
cd /c9sdk
if [ !-d /home/vagrant/workspace ]; then
  mkdir /home/vagrant/workspace
fi
node /c9sdk/server.js -l 0.0.0.0 -a : -w /home/vagrant/workspace
