VAGRANTFILE_API_VERSION = "2"
VAGRANT_NETWORK_IP = "192.168.56.199"

Vagrant.require_version ">= 1.7.0"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |c|
  c.vm.box = "ubuntu/trusty64"
  c.vm.hostname = "djember.dev"

  # Ports forwarding for manual run
  c.vm.network(:forwarded_port, {:guest=>8181, :host=>8181})  # C9 editor
  c.vm.network(:forwarded_port, {:guest=>8000, :host=>8000})  # Django
  c.vm.network(:forwarded_port, {:guest=>4200, :host=>4200})  # Ember

  c.vm.network :private_network, ip: VAGRANT_NETWORK_IP

  c.vm.synced_folder ".", "/vagrant", mount_options: ['dmode=777','fmode=755']

  c.vm.provider :virtualbox do |p|
    host = RbConfig::CONFIG['host_os']

    # https://stefanwrobel.com/how-to-make-vagrant-performance-not-suck
    # Give VM 1/4 system memory & access to all cpu cores on the host
    if host =~ /darwin/
      cpus = `sysctl -n hw.ncpu`.to_i
      # sysctl returns Bytes and we need to convert to MB
      mem = `sysctl -n hw.memsize`.to_i / 1024 / 1024 / 4
      c.vm.network(:forwarded_port, {:guest=>49152, :host=>49152})  # Ember live reload
    elsif host =~ /linux/
      cpus = `nproc`.to_i
      # meminfo shows KB and we need to convert to MB
      mem = `grep 'MemTotal' /proc/meminfo | sed -e 's/MemTotal://' -e 's/ kB//'`.to_i / 1024 / 4
      c.vm.network(:forwarded_port, {:guest=>49152, :host=>49152})  # Ember live reload
    else # sorry Windows folks, I can't help you
      # On windows 49152 is used by wininit.exe, we will use 8089 for Ember live reload
      c.vm.network(:forwarded_port, {:guest=>8089, :host=>8089})  # Ember live reload
      cpus = 2
      mem = 2560
    end

    p.customize ["modifyvm", :id, "--memory", mem]
    p.customize ["modifyvm", :id, "--cpus", cpus]
    p.customize ["setextradata", :id, "VBoxInternal2/SharedFoldersEnableSymlinksCreate/v-root", "1"]
  end

  c.vm.provision "shell" do |s|
    s.inline = "/vagrant/deployment/bootstrap.sh"
    s.privileged = true
  end

  c.vm.provision "shell" do |s|
    s.inline = "/vagrant/deployment/c9.sh"
    s.privileged = false
  end

  c.vm.provision "shell" do |s|
    s.inline = "/vagrant/deployment/supervisor.sh"
    s.privileged = true
  end

end
